// NormalFormat
// http://www.gnu.org/software/diffutils/manual/html_node/Example-Normal.html#Example-Normal
function GnuNormalFormat(change) {
	const nf = [];
	const str = [];

	// del add description
	// 0   >0  added count to rhs
	// >0  0   deleted count from lhs
	// >0  >0  changed count lines
	let op;
	if (change.lhs.del === 0 && change.rhs.add > 0) {
		op = 'a';
		return ''
	}
	else if (change.lhs.del > 0 && change.rhs.add === 0) {
		op = 'd';
		return ''
	}
	else {
		op = 'c';
	}

	function encodeSide(side, key) {
		// encode side as a start,stop if a range
		str.push(side.at + 1);
		if (side[key] > 1) {
			str.push(',');
			str.push(side[key] + side.at);
		}
	}
	encodeSide(change.lhs, 'del');
	str.push(op);
	encodeSide(change.rhs, 'add');

	nf.push(str.join(''));
	var nf0 = []
	const sp = ' ' || ''
	for (let i = change.lhs.at; i < change.lhs.at + change.lhs.del; ++i) {
		nf0.push(change.lhs.getPart(i).text + sp);
	}
	if (change.rhs.add && change.lhs.del) {
		nf0.push('---');
	}
	for (let i = change.rhs.at; i < change.rhs.at + change.rhs.add; ++i) {
		nf0.push(sp + change.rhs.getPart(i).text);
	}
	nf.push(nf0.join(''))
	return nf.join('\n');
	// return nf0.join('')
}

var formats = {
	GnuNormalFormat: function (changes) {
		var i, out = [];
		for (i = 0; i < changes.length; ++i) {
			out.push(GnuNormalFormat(changes[i]));
		}
		return out.join('\n');
	}
}

module.exports = formats;
