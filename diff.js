// const myers = require('myers-diff');
const myers = require('./src/');
const fs = require('fs')

// const lhs = fs.readFileSync('./tmp/outen.md', {encoding:'utf8', flag:'r'})
// const rhs = fs.readFileSync('./tmp/su.txt', {encoding:'utf8', flag:'r'})

const lhs = fs.readFileSync('./tmp/out.md', {encoding:'utf8', flag:'r'})
const rhs = fs.readFileSync('./tmp/br.txt', {encoding:'utf8', flag:'r'})

const diff = myers.diff(lhs, rhs, {
    // compare: 'chars',
    compare: 'words',
    ignoreWhitespace: true,
    ignoreCase: true,
    ignoreAccents: true,
    ignorePunctuation: true
})

console.log(myers.formats.GnuNormalFormat(diff));
// console.log(diff);

// const str = ''
// const segmenterJa = new Intl.Segmenter('zh-CN', { granularity: 'word' })
// const segments = segmenterJa.segment(str)
// console.table(Array.from(segments));

// const str = 'We believed in Jesus Christ, who died for us sins, who rose again.'
// const segmenterJa = new Intl.Segmenter('en-US', { granularity: 'word' })
// const segments = segmenterJa.segment(str)
// console.table(Array.from(segments).filter(it => it.isWordLike));